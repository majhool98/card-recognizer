package systems.bentoak.cardrecognizer.util

import systems.bentoak.cardrecognizer.R
import java.util.regex.Pattern

object Utils {

    fun bankLogoByItsPattern(str: String): Int {
        val visaCardPattern = Pattern.compile("^4[0-9]{6,}\$")
        val americanExpressPattern = Pattern.compile("^3[47][0-9]{5,}\$")
        val discoverPattern = Pattern.compile("^6(?:011|5[0-9]{2})[0-9]{3,}\$")
        val jcbPattern = Pattern.compile("^(?:2131|1800|35[0-9]{3})[0-9]{3,}\$")
        val dinersClubPattern = Pattern.compile("^3(?:0[0-5]|[68][0-9])[0-9]{4,}\$")
        val masterCardPattern = Pattern.compile("^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}\$")

        return when {
            visaCardPattern.matcher(str).matches() -> R.drawable.visa
            americanExpressPattern.matcher(str).matches() -> R.drawable.americanexpress
            discoverPattern.matcher(str).matches() -> R.drawable.discover
            jcbPattern.matcher(str).matches() -> R.drawable.jcb
            dinersClubPattern.matcher(str).matches() -> R.drawable.diners_club
            masterCardPattern.matcher(str).matches() -> R.drawable.mastercard
            else -> 0
        }
    }
}