package systems.bentoak.cardrecognizer.util

import android.annotation.SuppressLint
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.core.text.isDigitsOnly
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.huawei.hms.mlsdk.MLAnalyzerFactory
import com.huawei.hms.mlsdk.common.MLFrame
import systems.bentoak.cardrecognizer.R
import javax.inject.Inject

class CameraTextAnalyzer @Inject constructor() : ImageAnalysis.Analyzer {

    var function: ((String, String) -> Unit)? = null
    var number = ""
    var expiryDate = ""

    private fun degreesToFirebaseRotation(degrees: Int): Int = when(degrees) {
        0 -> FirebaseVisionImageMetadata.ROTATION_0
        90 -> FirebaseVisionImageMetadata.ROTATION_90
        180 -> FirebaseVisionImageMetadata.ROTATION_180
        270 -> FirebaseVisionImageMetadata.ROTATION_270
        else -> throw Exception("Rotation must be 0, 90, 180, or 270.")
    }

    @SuppressLint("UnsafeExperimentalUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image
        val imageRotation = degreesToFirebaseRotation(imageProxy.imageInfo.rotationDegrees)
        if (mediaImage != null) {

            val analyzer = MLAnalyzerFactory.getInstance().localTextAnalyzer
            val image = FirebaseVisionImage.fromMediaImage(mediaImage, imageRotation)
            val mlFrame
            = MLFrame.Creator().setBitmap(image.bitmap).create()

            analyzer
                .asyncAnalyseFrame(mlFrame)
                .addOnSuccessListener {
                    it.blocks.forEach { block ->
                        val trimmed = block.stringValue.replace(" ", "")

                        //recognize number
                        if (trimmed.length == 16 && trimmed.isDigitsOnly()) {
                            number = block.stringValue

                            if (Utils.bankLogoByItsPattern(trimmed) == R.drawable.americanexpress) {
                                if (function != null) {
                                    function!!(block.stringValue, expiryDate)
                                    analyzer.close()
                                }
                                return@addOnSuccessListener
                            }
                        }

                        //recognize expiry date
                        if (trimmed.contains("/") && trimmed.length > 4) {
                            try {
                                trimmed.replace("o", "0", true)
                                trimmed.replace("i", "1", true)

                                val year = trimmed[trimmed.indexOf('/') - 2].toString() + trimmed[trimmed.indexOf('/') - 1].toString()
                                val slash = trimmed[trimmed.indexOf('/')]
                                val month = trimmed[trimmed.indexOf('/') + 1].toString() + trimmed[trimmed.indexOf('/') + 2].toString()
                                if (year.isDigitsOnly() && month.isDigitsOnly()) {
                                    expiryDate = year + slash + month
                                }
                            } catch (e: Exception) {}
                        }
                    }

                    if (number.isNotEmpty() && expiryDate.isNotEmpty()) {
                        if (function != null) {
                            function!!(number, expiryDate)
                            analyzer.close()
                            return@addOnSuccessListener
                        }
                    }

                    imageProxy.close()

                }.addOnFailureListener { imageProxy.close() }
        }
    }

    fun setCallback(function: (String, String) -> Unit) {
        this.function = function
    }
}