package systems.bentoak.cardrecognizer.util

import android.app.Dialog
import android.os.Bundle
import android.view.Window
import android.widget.DatePicker
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import systems.bentoak.cardrecognizer.R


class DatePickerDialog(val listener: (Int, Int) -> Unit): DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.pick_date_layout)

        val datePicker = dialog.findViewById<DatePicker>(R.id.datePicker1)
        val ok = dialog.findViewById<TextView>(R.id.ok_date_picker)
        val cancel = dialog.findViewById<TextView>(R.id.cancel_date_picker)

        ok.setOnClickListener {
            listener(datePicker.year, datePicker.month)
            dialog.cancel()
        }

        cancel.setOnClickListener {
            dialog.cancel()
        }

        return dialog
    }
}