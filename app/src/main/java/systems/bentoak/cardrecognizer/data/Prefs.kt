package systems.bentoak.cardrecognizer.data

import android.content.Context
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class Prefs(context: Context) {

    private val sh by lazy {

        val masterKey = if (Build.VERSION.SDK_INT >= 23) {
            val spec = KeyGenParameterSpec.Builder("CardRecognizer", KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setKeySize(256)
                    .build()
            MasterKey.Builder(context, "CardRecognizer").setKeyGenParameterSpec(spec).build()
        } else {
            MasterKey.Builder(context, "CardRecognizer").build()
        }
        EncryptedSharedPreferences.create(
                context,
                "CardRecognizer",
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }
    private val editor = sh.edit()

    private val cardNumberKey = "key:cardNumber"
    private val cvvKey = "key:cvvKey"
    private val expiryDateKey = "key:expiryDate"

    var cardNumber: String
        get() = sh.getString(cardNumberKey, "")!!
        set(value) = editor.putString(cardNumberKey, value).apply()

    var cvv: String
        get() = sh.getString(cvvKey, "")!!
        set(value) = editor.putString(cvvKey, value).apply()

    var expiryDate: String
        get() = sh.getString(expiryDateKey, "")!!
        set(value) = editor.putString(expiryDateKey, value).apply()
}