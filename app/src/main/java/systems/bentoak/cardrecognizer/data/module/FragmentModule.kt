package systems.bentoak.cardrecognizer.data.module

import android.content.Context
import android.os.Handler
import android.os.Looper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import systems.bentoak.cardrecognizer.App

@Module
@InstallIn(FragmentComponent::class)
class FragmentModule {

    @Provides
    fun provideMainHandler() = Handler(Looper.getMainLooper())
}