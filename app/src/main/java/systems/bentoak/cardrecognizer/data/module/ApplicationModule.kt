package systems.bentoak.cardrecognizer.data.module

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import systems.bentoak.cardrecognizer.data.Prefs
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun providePreference(@ApplicationContext context: Context) = Prefs(context)
}