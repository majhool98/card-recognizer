package systems.bentoak.cardrecognizer.ui.card_details

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_card_details.*
import systems.bentoak.cardrecognizer.R
import systems.bentoak.cardrecognizer.ui.camera.CameraFragment
import systems.bentoak.cardrecognizer.util.*
import javax.inject.Inject


@AndroidEntryPoint
class CardDetailsFragment @Inject constructor(): Fragment(R.layout.fragment_card_details) {

    val viewModel by viewModels<CardDetailsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        expireDateText.setOnClickListener {
            DatePickerDialog { year, month ->

                expireDateText.text = ("${year.toString().substring(2, 4)}/${"%02d".format(month + 1)}")

            }.show(childFragmentManager, "tag")
        }

        cardNumberEditText.doAfterTextChanged {
            if (it != null) {
                val text = it.toString().replace(" ", "")
                if (text.length == 16) {

                    val drawable = Utils.bankLogoByItsPattern(text)
                    if (drawable == 0)
                        cardLogo.setImageDrawable(null)
                    else
                        cardLogo.setImageDrawable(ContextCompat.getDrawable(requireActivity(), drawable))
                } else {
                    cardLogo.setImageDrawable(null)
                }
            }
        }

        openCameraButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {

                openCamera()

            } else {
                requestPermissions(arrayOf(android.Manifest.permission.CAMERA),
                        Constants.CAMERA_REQUEST_PERMISSION_CODE)
            }
        }
    }

    private fun openCamera() {
        hideKeyboard()

        val cameraFragment = CameraFragment { number, expiry ->
            cardNumberEditText.setText(number)
            expireDateText.text = expiry
        }
        requireActivity()
                .supportFragmentManager
                .beginTransaction()
                .add(R.id.mainFrame, cameraFragment)
                .addToBackStack(null)
                .commit()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.CAMERA_REQUEST_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            }
        }
    }
}