package systems.bentoak.cardrecognizer.ui.camera

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.view.animation.*
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.camera_fragment.*
import systems.bentoak.cardrecognizer.R
import systems.bentoak.cardrecognizer.util.CameraTextAnalyzer
import systems.bentoak.cardrecognizer.util.viewModels
import java.util.concurrent.Executors
import javax.inject.Inject

@AndroidEntryPoint
class CameraFragment(val cardDetailsCallback: ((String, String) -> Unit)) : Fragment(R.layout.camera_fragment) {

    lateinit var cameraProvider: ProcessCameraProvider
    private val analyzer = ImageAnalysis.Builder().build()
    @Inject lateinit var imageCameraTextAnalyzer: CameraTextAnalyzer
    @Inject lateinit var mainHandler: Handler
    private val executor = Executors.newSingleThreadExecutor()
    val viewModel by viewModels<CameraViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator()
        fadeIn.duration = 1000

        val fadeOut = AlphaAnimation(1f, 0f)
        fadeOut.interpolator = AccelerateInterpolator()
        fadeOut.startOffset = 1000
        fadeOut.duration = 1000

        val animation = AnimationSet(false)
        animation.addAnimation(fadeIn)
        animation.addAnimation(fadeOut)

        recycleAnimation(animation)

        imageCameraTextAnalyzer.setCallback { number, expiry ->

            cardDetailsCallback(number, expiry)
            requireActivity().supportFragmentManager.popBackStack()
        }

        setUpCameraX()
    }

    private fun recycleAnimation(animation: AnimationSet) {

        if (cameraFrame == null) return

        cameraFrame.startAnimation(animation)

        mainHandler.postDelayed({
            recycleAnimation(animation)
        }, 2000)
    }

    private fun setUpCameraX() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            cameraProvider = cameraProviderFuture.get()

            requireActivity().runOnUiThread {

                val preview = Preview.Builder().build()
                preview.setSurfaceProvider(cameraPreview.surfaceProvider)

                try {
                    // Unbind use cases before rebinding
                    cameraProvider.unbindAll()
                    analyzer.setAnalyzer(executor, imageCameraTextAnalyzer)
                    // Bind use cases to camera
                    cameraProvider.bindToLifecycle(
                            this,
                            CameraSelector.DEFAULT_BACK_CAMERA,
                            preview,
                            analyzer
                    )

                } catch (exc: Exception) {
                    Log.e("TAG", "Use case binding failed", exc)
                }
            }
        }, executor)
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraProvider.unbindAll()
        executor.shutdown()
    }
}