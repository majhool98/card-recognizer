package systems.bentoak.cardrecognizer.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import systems.bentoak.cardrecognizer.R
import systems.bentoak.cardrecognizer.ui.card_details.CardDetailsFragment
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var cardDetailsFragment: CardDetailsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainFrame, cardDetailsFragment)
            .commit()
    }
}